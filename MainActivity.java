package com.example.webview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private WebView paginaweb;
    private TextView nombre;
    private ProgressBar barra;

    private Button atras,adelante,recarga,cerrar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        final WebView paginaweb = findViewById(R.id.paginaweb);
       TextView titulo = findViewById(R.id.titulo);

        Button atras=findViewById(R.id.atras);
        Button adelante=findViewById(R.id.adelante);
        final Button recarga=findViewById(R.id.recargar);
        Button cerrar= findViewById(R.id.cerrar);




        WebSettings webSettings = paginaweb.getSettings();
        webSettings.setJavaScriptEnabled(true);
        paginaweb.loadUrl("https://www.solobasket.com/");



//no me aparecia el nombre con el get title y lo puse a mano
      String nombreweb= paginaweb.getTitle();
      titulo.setText(nombreweb);
      titulo.setText(paginaweb.getTitle());

     titulo.setText("SOLOBASKET");



        // Forzamos el webview para que abra los enlaces internos dentro de la la APP
        paginaweb.setWebViewClient(new WebViewClient());
        paginaweb.setWebChromeClient(new WebChromeClient());


        //barra progreso
         final ProgressBar splash_screenProgressBar;
         final int MAX_VALUE = 30; // Aqui puedes poner el valor que desees
        splash_screenProgressBar = (ProgressBar) findViewById(R.id.splash_screenProgressBar);
        splash_screenProgressBar.setMax(MAX_VALUE);

        new CountDownTimer(6000, 20) {

            int progreso = 1; // Variable que va a ir aumentando del progreso
            @Override
            public void onTick(long millisUntilFinished) {
                splash_screenProgressBar.setProgress(progreso);
                progreso += (2);
            }

            @Override
            public void onFinish() {
                splash_screenProgressBar.setProgress(MAX_VALUE);
            }
        }.start();





//boton atras
atras.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        if (paginaweb.canGoBack()) {
            paginaweb.goBack();

            //barra progreso
            final ProgressBar splash_screenProgressBar;
            final int MAX_VALUE = 30; // Aqui puedes poner el valor que desees
            splash_screenProgressBar = (ProgressBar) findViewById(R.id.splash_screenProgressBar);
            splash_screenProgressBar.setMax(MAX_VALUE);

            new CountDownTimer(6000, 20) {

                int progreso = 1; // Variable que va a ir aumentando del progreso
                @Override
                public void onTick(long millisUntilFinished) {
                    splash_screenProgressBar.setProgress(progreso);
                    progreso += (2);
                }

                @Override
                public void onFinish() {
                    splash_screenProgressBar.setProgress(MAX_VALUE);
                }
            }.start();
        }else{
            Toast.makeText(MainActivity.this, "No hay historial de navegación", Toast.LENGTH_SHORT).show();
        }




    }

});

//boton adelante
adelante.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {


        if (paginaweb.canGoForward()) {
            paginaweb.goForward();

            //barra progreso
            final ProgressBar splash_screenProgressBar;
            final int MAX_VALUE = 30; // Aqui puedes poner el valor que desees
            splash_screenProgressBar = (ProgressBar) findViewById(R.id.splash_screenProgressBar);
            splash_screenProgressBar.setMax(MAX_VALUE);

            new CountDownTimer(6000, 20) {

                int progreso = 1; // Variable que va a ir aumentando del progreso
                @Override
                public void onTick(long millisUntilFinished) {
                    splash_screenProgressBar.setProgress(progreso);
                    progreso += (2);
                }

                @Override
                public void onFinish() {
                    splash_screenProgressBar.setProgress(MAX_VALUE);
                }
            }.start();
        }else{
            Toast.makeText(MainActivity.this, "No hay historial de navegación", Toast.LENGTH_SHORT).show();
        }



    }
});

//boton recarga
recarga.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {


        paginaweb.reload();
        paginaweb.clearHistory();

        //barra progreso
        final ProgressBar splash_screenProgressBar;
        final int MAX_VALUE = 30; // Aqui puedes poner el valor que desees
        splash_screenProgressBar = (ProgressBar) findViewById(R.id.splash_screenProgressBar);
        splash_screenProgressBar.setMax(MAX_VALUE);

        new CountDownTimer(6000, 20) {

            int progreso = 1; // Variable que va a ir aumentando del progreso
            @Override
            public void onTick(long millisUntilFinished) {
                splash_screenProgressBar.setProgress(progreso);
                progreso += (2);
            }

            @Override
            public void onFinish() {
                splash_screenProgressBar.setProgress(MAX_VALUE);
            }
        }.start();



    }
});

//boton cerrar
cerrar.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {


        paginaweb.stopLoading();
    }
});


    }
}
