package com.example.listados;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Pelicula implements Parcelable {


            private String nombre;
            private int año, caratula;
            private ArrayList<String> actores;

    public Pelicula(String nombre, String dorsal, String posicion) {
        this.nombre = nombre;
        this.caratula = caratula;
        this.año = año;
        this.actores=actores;

    }

    protected Pelicula(Parcel in) {
        nombre = in.readString();

    }

    public static final Creator<Pelicula> CREATOR = new Creator<Pelicula>() {
        @Override
        public Pelicula createFromParcel(Parcel in) {
            return new Pelicula(in);
        }

        @Override
        public Pelicula[] newArray(int size) {
            return new Pelicula[size];
        }
    };

    public String getNombre() {
        return nombre;
    }

    public int getAño() {
        return año;
    }

    public int getCaratula() {
        return caratula;
    }

    public ArrayList<String> getActores() {
        return actores;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nombre);
        dest.writeInt(caratula);
        dest.writeInt(año);

    }
}
