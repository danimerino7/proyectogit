package com.example.listados;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class ListadePelis extends AppCompatActivity {


    private ListView listView;
    private String [] reparto = {"Actor1","Actor2","Actor3","Director"};
    private java.util.ArrayList<Pelicula> ArrayList;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.lista_peliculas);


        ArrayList = new ArrayList<>();


        ArrayList.add(new Pelicula("","","");




        MyAdapter adapter = new MyAdapter(ArrayList);

        listView=findViewById(R.id.lista);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pelicula pelicula= ArrayList.get(position);
                Toast.makeText(ListadePelis.this, "Posicion"+position, Toast.LENGTH_SHORT).show();
                Intent intent= new Intent(ListadePelis.this,FichaJugador.class);
                intent.putExtra("jugador",pelicula);
                startActivity(intent);
            }
        });

    }
    }