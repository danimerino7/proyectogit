package com.example.listados;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class FichaPelicula extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_peliculas);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null && bundle.containsKey("pelicula")) {
            Pelicula pelicula = (Pelicula) bundle.getParcelable("pelicula");
            Toast.makeText(this, "Pelicula"+pelicula.getNombre(), Toast.LENGTH_SHORT).show();
        }

    }
}
