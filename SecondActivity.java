package com.example.conversormedidas;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    private TextView medidaorigen;
    private TextView unidadorigen;
    private TextView medidaaconvertir;
    private TextView unidadresultante;
    private DatePicker calendario;
    private TimePicker reloj;
    private Button correo;

    public SecondActivity() {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pantalladatos);


        medidaorigen=findViewById(R.id.medidaorigen);
        medidaaconvertir=findViewById(R.id.medidaaconvertir);
        unidadorigen= findViewById(R.id.unidadorigen);
        unidadresultante = findViewById(R.id.unidadresultante);
        calendario = findViewById(R.id.calendario);
        reloj = findViewById(R.id.reloj);
        correo = findViewById(R.id.correo);





        //recoge la informacion extra del intent
        Bundle bundle= getIntent().getExtras();
        final String datos =bundle.getString("datos");
        final String cifra =bundle.getString("unidadorigen");
        final String medida1=bundle.getString("medida1");
        final String medida2=bundle.getString("medida2");


        medidaorigen.setText(""+medida1);
        unidadorigen.setText(""+cifra);
        medidaaconvertir.setText(""+medida2);
        unidadresultante.setText(""+datos);


        correo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String hora;
                String minutos;
                String fecha;
              hora=  reloj.getCurrentHour().toString();
              minutos= reloj.getCurrentMinute().toString();

              calendario.getDayOfMonth();
              

                Intent intentcorreo = new Intent(Intent.ACTION_SENDTO);
                intentcorreo.putExtra(Intent.EXTRA_EMAIL, ""+medida1+""+cifra+"en"+medida2+"son"+datos);







            }
        });



    }
}
